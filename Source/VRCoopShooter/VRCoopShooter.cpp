// Copyright Epic Games, Inc. All Rights Reserved.

#include "VRCoopShooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VRCoopShooter, "VRCoopShooter" );
